using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClydeChaseState : GhostBaseState
{
    public float speedMultiplier = 1.0f;
    private bool beginRun = false;

    public float timeUntilScatter = 5.0f;
    private float timeElapsed = 0f;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        ghost.SetMoveToLocation(ghost.PacMan.position);
        ghost.speedModifier = speedMultiplier;
        beginRun = false;
        timeElapsed = 0;
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateExit(animator, stateInfo, layerIndex);
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        timeElapsed += Time.deltaTime;
        if (beginRun)
        {
            animator.SetTrigger("runaway");
        }
        else if (timeElapsed >= timeUntilScatter)
        {
            animator.SetTrigger("scatter");
        }
    }

    public override void OnPathCompleted()
    {
        ghost.SetMoveToLocation(ghost.PacMan.position);
    }

    public override void OnGameStateChanged(GameDirector.States _state)
    {
        if (_state == GameDirector.States.enState_PacmanInvincible)
        {
            beginRun = true;
        }
    }
}
