using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TestBehaviour : MonoBehaviour
{
    //public UnityEvent evt;
    //public GameDirector.GameStateChangedEvent stateChangedEvent;

    private void Start()
    {
        GameDirector.Instance.GameStateChanged.AddListener(TestMethod);
    }

    void TestMethod(GameDirector.States state)
    {
        Debug.Log(state.ToString());
    }
}
