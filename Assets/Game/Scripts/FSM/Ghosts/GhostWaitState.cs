using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostWaitState : GhostBaseState
{
    public float waitTimer = 5.0f;
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateExit(animator, stateInfo, layerIndex);
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        waitTimer -= Time.deltaTime;
        if (waitTimer <= 0)
        {
            animator.SetTrigger("begin_state");
        }
    }
}
