using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClydeScatter : GhostBaseState
{
    private bool beginChase = false;
    private bool beginRun = false;
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        ghost.SetMoveToLocation(ghost.ScatterLocation.position);
        beginChase = false;
        beginRun = false;
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateExit(animator, stateInfo, layerIndex);
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (beginRun)
        {
            animator.SetTrigger("runaway");
        }
        else if (beginChase)
        {
            animator.SetTrigger("chase");
        }
    }

    public override void OnPathCompleted()
    {
        beginChase = true;
    }

    public override void OnGameStateChanged(GameDirector.States _state)
    {
        if (_state == GameDirector.States.enState_PacmanInvincible)
        {
            beginRun = true;
        }
    }
}
