using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostBaseState : FSMBaseState
{
	protected GhostController ghost;
    protected GameDirector.States currentGameState;
	public override void Init(GameObject _owner, FSM _fsm)
	{
		base.Init(_owner, _fsm);
        ghost = _owner.GetComponent<GhostController>();
	}

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        GameDirector.Instance.GameStateChanged.AddListener(OnGameStateChanged);
        ghost.deathEvent.AddListener(OnDeath);
        ghost.pathCompletedEvent.AddListener(OnPathCompleted);
        ghost.moveCompletedEvent.AddListener(OnMoveCompleted);
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        GameDirector.Instance.GameStateChanged.RemoveListener(OnGameStateChanged);
        ghost.deathEvent.RemoveListener(OnDeath);
        ghost.pathCompletedEvent.RemoveListener(OnPathCompleted);
        ghost.moveCompletedEvent.RemoveListener(OnMoveCompleted);
    }

    public virtual void OnPathCompleted()
    {}

	public virtual void OnDeath()
    {}

	public virtual void OnMoveCompleted()
    {}

    public virtual void OnGameStateChanged(GameDirector.States _state)
    {}
}
