using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InkyChaseState : GhostBaseState
{
    public float speedMultiplier = 1.0f;
    public float speedIncrementPerPellet = 0.01f;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        GameDirector.Instance.collectedPelletEvent.AddListener(IncreaseSpeed);
        ghost.SetMoveToLocation(ghost.PacMan.position);
        ghost.speedModifier = speedMultiplier;
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateExit(animator, stateInfo, layerIndex);
        GameDirector.Instance.collectedPelletEvent.RemoveListener(IncreaseSpeed);
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (GameDirector.Instance.state == GameDirector.States.enState_PacmanInvincible)
        {
            animator.SetTrigger("runaway");
        }
    }

    public override void OnPathCompleted()
    {
        ghost.SetMoveToLocation(ghost.PacMan.position);
    }

    public void IncreaseSpeed(int value)
    {
        speedMultiplier += speedIncrementPerPellet;
        ghost.speedModifier = speedMultiplier;
    }
}
