using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostDeathState : GhostBaseState
{
    public float deathSpeedModifier = 2.0f;
    private bool readyRespawn = false;
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        ghost.SetMoveToLocation(ghost.ReturnLocation);
        ghost.speedModifier = deathSpeedModifier;
        readyRespawn = false;
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateExit(animator, stateInfo, layerIndex);
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (readyRespawn)
        {
            ghost.speedModifier = 1.0f;
            animator.SetTrigger("resurrect");
        }
    }

    public override void OnPathCompleted()
    {
        Debug.Log("Respawn ready");
        readyRespawn = true;
    }
}
