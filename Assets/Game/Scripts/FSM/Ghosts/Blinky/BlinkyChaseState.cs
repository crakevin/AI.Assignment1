using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlinkyChaseState : GhostBaseState
{
    public float speedMultiplier = 1.0f;
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex); //Registers all the overrided callbacks

        ghost.SetMoveToLocation(ghost.PacMan.position);
        ghost.speedModifier = speedMultiplier;
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateExit(animator, stateInfo, layerIndex); //Unregister all the overrided callbacks
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (GameDirector.Instance.state == GameDirector.States.enState_PacmanInvincible)
        {
            animator.SetTrigger("runaway");
        }
    }

    public override void OnMoveCompleted()
    {
        //If pacman is further than 3 units from the ghosts current target, update the target
        if (Vector2.Distance(ghost.PacMan.position, ghost.GetMoveLocation()) > 3.0f)
        {
            ghost.SetMoveToLocation(ghost.PacMan.position);
        }
    }

    public override void OnPathCompleted()
    {
        ghost.SetMoveToLocation(ghost.PacMan.position);
    }
}