using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostResurrect : GhostBaseState
{
    public float RespawnTimerSeconds = 5.0f;
    public float CurrentTimer;
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        CurrentTimer = RespawnTimerSeconds;
        Debug.Log($"{ghost.name}: Resurrect state entered");
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateExit(animator, stateInfo, layerIndex); //Unregister all the overrided callbacks
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        CurrentTimer -= Time.deltaTime;
        if(CurrentTimer <= 0)
        {
            ghost.Resurrect();
            CurrentTimer = RespawnTimerSeconds;
            animator.SetTrigger("chase");
        }
    }
}
