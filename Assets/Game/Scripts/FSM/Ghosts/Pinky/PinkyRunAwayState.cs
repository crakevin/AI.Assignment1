using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PinkyRunAwayState : GhostBaseState
{
    public float speedModifier = 2.0f;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex); //Registers all the overrided callbacks

        ghost.SetMoveToLocation(-ghost.PacMan.position); //Run to the other side of the map from where Pacman is
        ghost.speedModifier = speedModifier;
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateExit(animator, stateInfo, layerIndex); //Deregisters all the overrided callbacks
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (GameDirector.Instance.state == GameDirector.States.enState_Normal)
        {
            animator.SetTrigger("chase");
        }
        else if (ghost.isDead)
        {
            animator.SetTrigger("death");
        }
    }

    public override void OnPathCompleted()
    {
        //Simple move to the opposite side of the map
        ghost.SetMoveToLocation(-ghost.PacMan.position);
    }
}
