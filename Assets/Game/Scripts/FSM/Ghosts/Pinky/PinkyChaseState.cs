using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PinkyChaseState : GhostBaseState
{
    public float speedMultiplier = 1.0f;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        ghost.SetMoveToLocation(ghost.PacMan.position);
        ghost.speedModifier = speedMultiplier;
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateExit(animator, stateInfo, layerIndex);
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (GameDirector.Instance.state == GameDirector.States.enState_PacmanInvincible)
        {
            animator.SetTrigger("runaway");
        }
    }

    public override void OnPathCompleted()
    {
        int randXOffset = Random.Range(-2, 2);
        int randYOffset = Random.Range(-2, 2);
        Vector2 randVector = new Vector2(randXOffset, randYOffset);
        Vector2 pacmanPos = ghost.PacMan.position;
        ghost.SetMoveToLocation(pacmanPos + randVector);
    }
}
